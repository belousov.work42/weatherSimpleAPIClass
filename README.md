**Weather Class**

Цель данного проекта показать преобретенные навыки работы с gitlab CI/CD на примере проекта.
Данные проект реализует класс позволяющщий получить погоду с openweathermap.org.

Запуск:

    python -V  # Print out python version for debugging
    
    pip3 install virtualenv
    
    virtualenv venv
    
    source venv/bin/activate
    
    pip3 install -r requirements.txt
    
    python3 main.py