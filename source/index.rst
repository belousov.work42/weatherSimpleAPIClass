.. Weather documentation master file, created by
   sphinx-quickstart on Sun Dec 20 23:06:40 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Weather's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: weather_req
   :members:
   :undoc-members:
   :show-inheritance:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
