import requests

import requests



querystring = {"lat":"38.5","lon":"-78.5"}

headers = {
    'x-rapidapi-key': "SIGN-UP-FOR-KEY",
    'x-rapidapi-host': "weatherbit-v1-mashape.p.rapidapi.com"
    }

class WeatherRequester:
    """ It's simple WeatherRequester, main class
    :argument Optional key(can be in sys variables WEATHER_API_KEY
    """

    open_wether_link = 'http://api.openweathermap.org/data/2.5/weather?'
    weather_bit_url = "https://api.weatherbit.io/v2.0/current?"

    def __init__(self):
        # if (not api_key) or (not isinstance(api_key, str)):
        import os
        self.WEATHER_API_KEY = os.environ['WEATHER_API_KEY']
        self.WEATHER_BIT_API_KEY = os.environ['WEATHER_BIT_API_KEY']
        self.WEATHER_API_KEY_ARG = f'&appid={self.WEATHER_API_KEY}'
        self.WEATHER_BIT_API_KEY_ARG = f'&key={self.WEATHER_BIT_API_KEY}'

    def _get_main_info(self, open_weather_result, weather_bit_result):
        open_weather_result = {'data_from': 'open_weather', 'city': open_weather_result['name'], 'temp': open_weather_result['main']['temp'],
                  'weather': open_weather_result['weather'][0]['description']}
        weather_bit_result = {'data_from': 'weather_bit', 'city': weather_bit_result['data'][0]['city_name'], 'temp': weather_bit_result['data'][0]["temp"],
                              'weather': weather_bit_result['data'][0]['weather']}
        return open_weather_result, weather_bit_result

    def get_weather_in_city_info(self, city_name, main_info=True):
        """ It's simple WeatherRequester, main class
        :argument city name """
        open_weather_result = requests.get(f'{self.open_wether_link}q={city_name}{self.WEATHER_API_KEY_ARG}').json()
        weather_bit_result = requests.get(f'{self.weather_bit_url}&city={city_name.lower()}{self.WEATHER_BIT_API_KEY_ARG}', headers={'x-rapidapi-host': "weatherbit-v1-mashape.p.rapidapi.com"}).json()
        if main_info:
            open_weather_result, weather_bit_result = self._get_main_info(open_weather_result, weather_bit_result)
        return open_weather_result, weather_bit_result
